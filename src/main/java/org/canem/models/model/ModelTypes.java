package org.canem.models.model;

import com.flowpowered.math.vector.Vector3d;

import java.util.Optional;

public enum ModelTypes {
    ITEM,
    BLOCK_SOUTH(-30, 0, 0),
    BLOCK_EAST(-30, 0, 0),
    BLOCK_NORTH(-30, 0, 0),
    BLOCK_WEST(-30, 0, 0),
    BLOCK_DIRECTIONAL(-30, 0, 0);

    ModelTypes() {}
    ModelTypes(double x, double y, double z) {
        this.direction = new Vector3d(x, y, z);
    }

    public Optional<Vector3d> getDirection() { return Optional.ofNullable(direction); }

    private Vector3d direction;
}
