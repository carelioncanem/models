package org.canem.models.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.MobSpawner;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.MobSpawnerData;
import org.spongepowered.api.data.property.item.UseLimitProperty;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.weighted.WeightedSerializableObject;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class Model {
    private final String path;
    private final ModelTypes type;
    private final ItemType itemType;
    private final Integer modelIdx;

    public Model(String path, ModelTypes type, ItemType itemType, Integer modelIdx) {
        this.path = path;
        this.type = type;
        this.itemType = itemType;
        this.modelIdx = modelIdx;
    }

    public void save(String dir) {
        JsonObject json = new JsonObject();
        json.add("parent", new JsonPrimitive(path));

        type.getDirection().ifPresent(direction -> {
            JsonObject display = new JsonObject(), head = new JsonObject();
            JsonArray translation = new JsonArray(), rotation = new JsonArray(), scale = new JsonArray();

            rotation.add(direction.getX());
            rotation.add(direction.getY());
            rotation.add(direction.getZ());

            translation.add(-7d);
            translation.add(6.2d);
            translation.add(2.25d);

            scale.add(3.75d);
            scale.add(3.75d);
            scale.add(3.75d);

            head.add("rotation", rotation);
            head.add("translation", translation);
            head.add("scale", scale);
            display.add("thirdperson_hat", head);
        });

        try {
            File file = new File(dir + File.separator + path + ".json");
            File modelDir = file.getParentFile();

            modelDir.mkdirs();
            file.createNewFile();

            FileWriter writer = new FileWriter(file);
            writer.write(json.toString());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ItemStack toItemStack() {
        return ItemStack.builder()
            .itemType(itemType)
            .add(Keys.UNBREAKABLE, true)
            .add(Keys.HIDE_UNBREAKABLE, true)
            .add(Keys.ITEM_DURABILITY, modelIdx)
            .add(Keys.HIDE_ATTRIBUTES, true)
            .build();
    }

    public void setBlockAt(Location<World> location) {
        int limit = itemType.getDefaultProperty(UseLimitProperty.class).get().getValue();
        DataContainer entityData = DataContainer.createNew();

        DataContainer head = DataContainer.createNew();
        DataContainer empty = DataContainer.createNew();

        head.set(DataQuery.of("id"), itemType.getId());
        head.set(DataQuery.of("Count"), (short)1);
        head.set(DataQuery.of("Damage"), (short)limit - modelIdx);
        head.set(DataQuery.of("tag"), DataContainer.createNew().set(DataQuery.of("Unbreakable"), (short)1));
        entityData.set(DataQuery.of("ArmorItems"), Arrays.asList(empty, empty, empty, head));
        entityData.set(DataQuery.of("Invisible"), (short)1);

        EntityArchetype archetype = EntityArchetype.builder()
                .type(EntityTypes.ARMOR_STAND)
                .entityData(entityData)
                .build();

        location.setBlockType(BlockTypes.MOB_SPAWNER);
        MobSpawner spawner = (MobSpawner)location.getTileEntity().get();
        MobSpawnerData data = spawner.getMobSpawnerData();

        data.set(Keys.SPAWNER_NEXT_ENTITY_TO_SPAWN, new WeightedSerializableObject<>(archetype, 1));
        data.set(Keys.SPAWNER_SPAWN_RANGE, (short)0);
        data.set(Keys.SPAWNER_REQUIRED_PLAYER_RANGE, (short)0);

        spawner.offer(data);
    }

    public String getPath() {
        return path;
    }

    public Integer getId() {
        return modelIdx;
    }
}
