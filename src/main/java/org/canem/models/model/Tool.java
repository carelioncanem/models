package org.canem.models.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.spongepowered.api.data.property.item.UseLimitProperty;
import org.spongepowered.api.item.ItemType;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Tool {
    private final Map<Integer, Model> models = new HashMap<>();
    private final ItemType type;
    private final boolean modifiable;
    private long durability = 0L;

    public Tool(ItemType type, long durability, boolean modifiable) {
        this.type = type;
        this.modifiable = modifiable;
        this.durability = durability;
    }

    public Tool(ItemType type, boolean modifiable) {
        this.type = type;
        this.modifiable = modifiable;

        type.getDefaultProperty(UseLimitProperty.class).ifPresent(limitProperty -> this.durability = limitProperty.getValue());
    }

    public void save(String dir) {
        String categoryName = type.getId().substring(type.getId().lastIndexOf(':') + 1);

        JsonObject textures = new JsonObject();
        textures.add("layer0", new JsonPrimitive("items/" + categoryName));

        JsonArray overrides = new JsonArray();
        for (Model model : models.values())
            overrides.add(this.getModelEntry(0, BigDecimal.valueOf(1d - (double) model.getId() / durability), "custom/" + model.getPath()));

        overrides.add(this.getModelEntry(1, 0, "item/" + categoryName));

        JsonObject json = new JsonObject();
        json.add("parent", new JsonPrimitive("item/handheld"));
        json.add("textures", textures);
        json.add("overrides", overrides);

        try(FileWriter file = new FileWriter(dir + File.separator + categoryName + ".json")) {
            file.write(json.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private JsonObject getModelEntry(int damaged, Number damage, String model) {
        JsonObject entry = new JsonObject();
        JsonObject predicate = new JsonObject();

        predicate.add("damaged", new JsonPrimitive(damaged));
        predicate.add("damage", new JsonPrimitive(damage));

        entry.add("predicate", predicate);
        entry.add("model", new JsonPrimitive(model));

        return entry;
    }

    public ItemType getType() {
        return type;
    }

    public Map<Integer, Model> getModels() {
        return models;
    }

    public boolean isModifiable() {
        return modifiable;
    }

    public long getDurability() {
        return durability;
    }
}
