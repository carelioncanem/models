package org.canem.models;

import ninja.leaping.configurate.objectmapping.Setting;
import org.canem.models.config.ModelEntry;
import org.canem.models.config.ModelsConfig;
import org.canem.models.config.ModelsData;
import org.canem.models.model.Model;
import org.canem.models.model.ModelTypes;
import org.canem.models.model.Tool;
import org.canem.models.util.Config;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.property.item.UseLimitProperty;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.util.rotation.Rotation;

import java.io.File;
import java.nio.file.Path;
import java.util.*;

public class ModelsService {
    private final ModelsData data;
    private final ModelsConfig config;
    private final Map<ItemType, Tool> tools = new HashMap<>();
    private final Map<String, Model> models = new HashMap<>();

    ModelsService(Logger logger) {
        PluginContainer container = Models.getInstance().getPluginContainer();

        data = new ModelsData(container, "data.bin");
        config = new ModelsConfig(container, "config.conf");

        //loading config
        for(String item : config.tools) {
            Optional<ItemType> itemType = Sponge.getRegistry().getType(ItemType.class, item);
            if(itemType.isPresent()) {
                Optional<UseLimitProperty> limitProperty = itemType.get().getDefaultProperty(UseLimitProperty.class);
                if(limitProperty.isPresent())
                    tools.put(itemType.get(), new Tool(itemType.get(), limitProperty.get().getValue(), true));
                else
                    logger.error("Item " + item + " is not a tool!");
            }
            else {
                logger.error("Item with id '" + item + "' does not exist!");
            }
        }

        //loading data
        for(Map.Entry<String, Map<Integer, ModelEntry>> toolEntry : data.models.entrySet()) {
            ItemType itemType = Sponge.getRegistry().getType(ItemType.class, toolEntry.getKey()).get();
            Tool tool = tools.get(itemType);

            if(tool == null)
                tools.put(itemType, tool = new Tool(itemType, false));

            for(Map.Entry<Integer, ModelEntry> id : toolEntry.getValue().entrySet()) {
                Model model = new Model(id.getValue().path, id.getValue().type, itemType, id.getKey());
                models.put(id.getValue().path, model);
                tool.getModels().put(id.getKey(), model);
            }
        }
    }

    /**
     * Saves all models in resource pack format (json files)
     */
    public void save() {
        Path configDir = Sponge.getConfigManager().getPluginConfig(Models.getInstance()).getDirectory();

        File modelsDir = configDir.resolve("workspace/compiled/assets/minecraft/models/custom").toFile();
        modelsDir.mkdirs();
        for(Model model : models.values())
            model.save(modelsDir.getPath());

        File itemsDir = configDir.resolve("workspace/compiled/assets/minecraft/models/item").toFile();
        itemsDir.mkdirs();
        for(Tool tool : tools.values())
            tool.save(itemsDir.getPath());
    }

    /**
     * Register model in the system
     * @param modelPath path to the model in resource pack
     * @param modelType type of model
     * IMPORTANT - The model will be saved differently in the system & resource pack, based on the @modeltype
     */
    public void register(String modelPath, ModelTypes modelType) {
        if(this.get(modelPath).isPresent())
            return;

        for (Tool tool : tools.values()) {
            if(!tool.isModifiable())
                continue;

            for (int idx = 1; idx < tool.getDurability(); idx++) {
                if (tool.getModels().containsKey(idx))
                    continue;

                Model model = new Model(modelPath, modelType, tool.getType(), idx);
                tool.getModels().put(idx, model);
                models.put(modelPath, model);

                data.models.computeIfAbsent(tool.getType().getId(), k -> new HashMap<>()).put(idx, new ModelEntry());
                data.save();

                if(modelType.equals(ModelTypes.BLOCK_DIRECTIONAL)) {
                    this.register(modelPath + "_east", ModelTypes.BLOCK_EAST);
                    this.register(modelPath + "_north", ModelTypes.BLOCK_NORTH);
                    this.register(modelPath + "_west", ModelTypes.BLOCK_WEST);
                }

                return;
            }
        }
    }

    /**
     * Gets model object of previously registered model
     * @param modelPath path to the model in resource pack
     * @return Model object
     */
    public Optional<Model> get(String modelPath) {
        return Optional.ofNullable(models.get(modelPath));
    }
}
