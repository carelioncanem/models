package org.canem.models.config;

import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.canem.models.util.Config;
import org.spongepowered.api.plugin.PluginContainer;

import java.util.HashMap;
import java.util.Map;

public class ModelsData extends Config {
    @Setting
    public Map<String, Map<Integer, ModelEntry>> models = new HashMap<>();

    public ModelsData(PluginContainer container, String path) {
        super(container, path);

        if(models == null)
            models = new HashMap<>();
    }
}
