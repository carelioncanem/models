package org.canem.models.config;

import ninja.leaping.configurate.objectmapping.Setting;
import org.canem.models.util.Config;
import org.spongepowered.api.plugin.PluginContainer;

import java.util.ArrayList;
import java.util.List;

public class ModelsConfig extends Config {
    @Setting(comment = "Tools used for custom models")
    public List<String> tools;

    public ModelsConfig(PluginContainer container, String path) {
        super(container, path);

        if(tools == null)
            tools = new ArrayList<>();
    }
}
