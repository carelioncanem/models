package org.canem.models.config;

import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.canem.models.model.ModelTypes;

@ConfigSerializable
public class ModelEntry {
    @Setting public String path;
    @Setting public ModelTypes type;

    public ModelEntry() {}
    public ModelEntry(String model, ModelTypes type) {
        this.path = model;
        this.type = type;
    }
}
