package org.canem.models;

import com.google.inject.Inject;
import org.canem.models.data.ModelDataKeys;
import org.canem.models.model.Model;
import org.canem.models.model.ModelTypes;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.DataRegistration;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.game.GameRegistryEvent;
import org.spongepowered.api.event.game.state.GamePostInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;

@Plugin(
        id = "models",
        name = "Models",
        description = "Models API",
        authors = {
                "Canem"
        }
)
public class Models {
    private static Models instance;
    private ModelsService service;

    @Inject
    private Logger logger;

    @Listener
    public void onKeyRegistration(GameRegistryEvent.Register<Key<?>> event) {
        ModelDataKeys.registerKeys();
        event.register(ModelDataKeys.MODELS);
    }

    @Listener
    public void onDataRegistration(GameRegistryEvent.Register<DataRegistration<?, ?>> event) {
        ModelDataKeys.registerData();
    }

    @Listener
    public void onServerPreInit(GamePreInitializationEvent event) {
        instance = this;
    }

    @Listener
    public void onServerPostInit(GamePostInitializationEvent event) {
        logger.info("Initializing API...");
        Sponge.getServiceManager().setProvider(this, ModelsService.class, service = new ModelsService(logger));

        CommandSpec packCommand = CommandSpec.builder()
                .executor(((src, args) -> {
                    service.save();
                    return CommandResult.success();
                }))
                .build();

        CommandSpec getCommand = CommandSpec.builder()
                .arguments(GenericArguments.string(Text.of("modelpath")))
                .executor(((src, args) -> {
                    if(src instanceof ConsoleSource) {
                        src.sendMessage(Text.of(TextColors.RED, "Only players can use this command!"));
                        return CommandResult.success();
                    }

                    args.<String>getOne("modelpath")
                            .flatMap(service::get)
                            .map(Model::toItemStack)
                            .ifPresent(((Player)src).getInventory()::offer);
                    return CommandResult.success();
                }))
                .build();

        Sponge.getCommandManager().register(this,
                CommandSpec.builder()
                    .permission("models.debug")
                    .child(packCommand, "pack")
                    .child(getCommand, "get")
                    .build(),
                "mdl", "models");

        service.register("items/testitem", ModelTypes.ITEM);
        service.register("items/testblock", ModelTypes.BLOCK_DIRECTIONAL);
    }

    @Listener
    public void onBlockInteract(InteractBlockEvent.Secondary event, @Root Player player) {
        player.getItemInHand((event instanceof InteractBlockEvent.Secondary.MainHand) ? HandTypes.MAIN_HAND : HandTypes.OFF_HAND)
            .flatMap(itemStack -> itemStack.get(ModelDataKeys.MODELS))
            .ifPresent(models -> {
                Optional<Location<World>> targetBlock = event.getTargetBlock().getLocation();

                if(targetBlock.isPresent()) {
                    final int directionId = (int)Math.floor((player.getHeadRotation().getY() * 4.0f / 360.0f) + 0.5d) & 3;
                    service.get(models.size() > 1 ? models.get(directionId) : models.get(0))
                            .ifPresent(model -> model.setBlockAt(targetBlock.get().getRelative(event.getTargetSide().getOpposite())));
                }
            });
    }

    public PluginContainer getPluginContainer() {
        return Sponge.getPluginManager().fromInstance(this)
                .orElseThrow(() -> new IllegalStateException("Could not access Models plugin container."));
    }

    public static Models getInstance() {
        return instance;
    }
}
