package org.canem.models.data;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.manipulator.DataManipulatorBuilder;
import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableData;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractData;
import org.spongepowered.api.data.merge.MergeFunction;
import org.spongepowered.api.data.persistence.AbstractDataBuilder;
import org.spongepowered.api.data.persistence.InvalidDataException;
import org.spongepowered.api.data.value.immutable.ImmutableListValue;
import org.spongepowered.api.data.value.mutable.ListValue;
import org.spongepowered.common.data.value.mutable.SpongeListValue;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class BlockModelData extends AbstractData<BlockModelData, BlockModelData.Immutable> {
    private List<String> models;

    public BlockModelData() {
        this.registerGettersAndSetters();
    }
    public BlockModelData(List<String> models) {
        this.models = models;
        this.registerGettersAndSetters();
    }
    public BlockModelData(String... models) {
        this.models = Arrays.asList(models);
        this.registerGettersAndSetters();
    }

    public ListValue<String> models() { return new SpongeListValue<>(ModelDataKeys.MODELS, models); }

    @Override
    protected void registerGettersAndSetters() {
        this.registerFieldGetter(ModelDataKeys.MODELS, () -> models);
        this.registerFieldSetter(ModelDataKeys.MODELS, models -> this.models = models);
        this.registerKeyValue(ModelDataKeys.MODELS, this::models);
    }

    @Override
    public Optional<BlockModelData> fill(DataHolder dataHolder, MergeFunction overlap) {
        BlockModelData merged = overlap.merge(this, dataHolder.get(BlockModelData.class).orElse(null));
        this.models = merged.models().get();

        return Optional.of(this);
    }

    @Override
    public Optional<BlockModelData> from(DataContainer container) {
        return from((DataView)container);
    }

    public Optional<BlockModelData> from(DataView dataView) {
        if(!dataView.contains(ModelDataKeys.MODELS.getQuery()))
            return Optional.empty();

        this.models = dataView.getStringList(ModelDataKeys.MODELS.getQuery()).get();
        return Optional.of(this);
    }

    @Override
    public BlockModelData copy() {
        return new BlockModelData(models);
    }

    @Override
    public Immutable asImmutable() {
        return new Immutable(models);
    }

    @Override
    public int getContentVersion() {
        return Builder.VERSION;
    }

    @Override
    public DataContainer toContainer() {
        return super.toContainer()
                .set(ModelDataKeys.MODELS, this.models);
    }

    public static class Immutable extends AbstractImmutableData<BlockModelData.Immutable, BlockModelData> {
        private List<String> models;

        public Immutable() { this.registerGetters(); }
        public Immutable(List<String> models) {
            this.models = models;
            this.registerGetters();
        }

        public ImmutableListValue<String> models() { return new SpongeListValue<>(ModelDataKeys.MODELS, models).asImmutable(); }

        @Override
        protected void registerGetters() {
            this.registerFieldGetter(ModelDataKeys.MODELS, () -> this.models);
            this.registerKeyValue(ModelDataKeys.MODELS, this::models);
        }

        @Override
        public BlockModelData asMutable() {
            return new BlockModelData(this.models);
        }

        @Override
        public int getContentVersion() {
            return Builder.VERSION;
        }

        @Override
        public DataContainer toContainer() {
            return super.toContainer()
                    .set(ModelDataKeys.MODELS, this.models);
        }
    }

    public static class Builder extends AbstractDataBuilder<BlockModelData> implements DataManipulatorBuilder<BlockModelData, Immutable> {
        public static final int VERSION = 1;

        public Builder() { super(BlockModelData.class, VERSION); }

        @Override
        public BlockModelData create() {
            return new BlockModelData();
        }

        @Override
        public Optional<BlockModelData> createFrom(DataHolder dataHolder) {
            return create().fill(dataHolder);
        }

        @Override
        protected Optional<BlockModelData> buildContent(DataView container) throws InvalidDataException {
            return create().from(container.copy());
        }
    }
}
