package org.canem.models.data;

import com.google.common.reflect.TypeToken;
import org.canem.models.Models;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataRegistration;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.value.mutable.ListValue;
import org.spongepowered.api.util.generator.dummy.DummyObjectProvider;

public class ModelDataKeys {
    public static Key<ListValue<String>> MODELS = DummyObjectProvider.createFor(Key.class, "MODELS");

    public static void registerKeys() {
        MODELS = Key.builder()
                .type(new TypeToken<ListValue<String>>(){})
                .id("models")
                .name("Models")
                .query(DataQuery.of("models"))
                .build();
    }

    public static void registerData() {
        DataRegistration.builder()
                .dataName("Models Data")
                .manipulatorId("models_data")
                .dataClass(BlockModelData.class)
                .immutableClass(BlockModelData.Immutable.class)
                .builder(new BlockModelData.Builder())
                .buildAndRegister(Models.getInstance().getPluginContainer());
    }
}
