package org.canem.models.util;

import ninja.leaping.configurate.SimpleConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMapper;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.asset.Asset;
import org.spongepowered.api.plugin.PluginContainer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public abstract class Config {
    private ObjectMapper<Config>.BoundInstance configMapper;
    private ConfigurationLoader<CommentedConfigurationNode> loader;

    public Config(PluginContainer container, String path) {
        File file = Sponge.getConfigManager().getPluginConfig(container).getDirectory().resolve(path).toFile();

        if(!file.exists()) {
            Optional<Asset> optionalAsset = container.getAsset(path);
            file.getParentFile().mkdirs();

            try {
                if(optionalAsset.isPresent())
                    optionalAsset.get().copyToDirectory(Paths.get(file.getParent()));
                else
                    file.createNewFile();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.loader = HoconConfigurationLoader.builder().setFile(file).build();
        try {
            this.configMapper = ObjectMapper.forObject(this);
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }

        this.load();
    }

    protected Config(ConfigurationLoader<CommentedConfigurationNode> loader) {
        this.loader = loader;
        try {
            this.configMapper = ObjectMapper.forObject(this);
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }

        this.load();
    }

    public void save() {
        try {
            SimpleConfigurationNode out = SimpleConfigurationNode.root();
            this.configMapper.serialize(out);
            this.loader.save(out);
        } catch (ObjectMappingException | IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        try {
            this.configMapper.populate(this.loader.load());
        } catch (ObjectMappingException | IOException e) {
            e.printStackTrace();
        }
    }
}
